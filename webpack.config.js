const webpack = require('webpack');
// webpack.config.js
module.exports = {
  entry: './src/main.js',
  output: {
    filename: './dist/build.js'
  },
  devtool: 'inline-source-map',
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.(jpg|png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url?limit=250000'
      },
      {
        test: /\.(woff|woff2)$/,
        loader: 'url?prefix=font/&limit=250000'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=250000&mimetype=application/octet-stream',
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=250000&mimetype=image/svg+xml'
      },
      {
        test: /\.jpg$/,
        loader: "file?name=[path][name].[ext]"
      },
      {
        test: /\.png$/,
        loader: "file?name=[path][name].[ext]"
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
        jQuery: "jquery",
        $: "jquery"
    })
  ],
  resolve: {
  }
}
