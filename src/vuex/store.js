import Vue from 'vue';
import Vuex from 'vuex';
import mutations from './mutation.js';

Vue.use(Vuex);

const state = {
  map:null
};

export default new Vuex.Store({
  state,
  mutations
});
